const t = require('@babel/types');

function replaceStringValue(node, value) {
  const { raw = `'${source}'` } = node.extra || {};
  const quote = raw[0];

  node.value = value;
  node.extra = {
    raw: `${quote}${value}${quote}`,
    rawValue: value,
  };
}

function jestifyImports() {
  return {
    ImportDeclaration(path) {
      const { source } = path.node;

      if (!source || !source.value) {
        return;
      }

      const newValue = source.value.replace(/^spec(\/helpers)?\//, 'helpers/');

      replaceStringValue(source, newValue);
    },
  };
};

module.exports = jestifyImports;
