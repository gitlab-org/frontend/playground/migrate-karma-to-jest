const path = require('path');
const {
  NodeJsInputFileSystem,
  CachedInputFileSystem,
  ResolverFactory 
} = require('enhanced-resolve');

let resolver;

function tryResolveConfigPath(configPath) {
  if (!configPath) {
    return {};
  }

  try {
    return require(path.resolve(configPath));
  } catch(e) {
    if (e.code !== 'MODULE_NOT_FOUND') {
      throw e;
    }

    return {};
  }
}

function init(configPath) {
  const { resolve: resolveConfig = {} } = tryResolveConfigPath(configPath);

  resolver = ResolverFactory.createResolver({
    extensions: ['.js', '.json'],
    ...resolveConfig,
    useSyncFileSystemCalls: true,
    fileSystem: new CachedInputFileSystem(new NodeJsInputFileSystem(), 4000),
  });
}

function resolve(sourcePath, requestPath) {
  if (!resolver) {
    return Promise.reject('You must call init first');
  }

  return resolver.resolveSync({}, sourcePath, requestPath);
};

module.exports = {
  init,
  resolve,
};
