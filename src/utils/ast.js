const t = require('@babel/types');
const { traverseFile } = require('../parser');

function isRHSOfMemberExpression(path, parentName) {
  if (!t.isMemberExpression(path.parent)) {
    return false;
  }

  return t.isMemberExpression(path.parent.object)
    ? t.isIdentifier(path.parent.object.property, { name: parentName })
    : t.isIdentifier(path.parent.object, { name: parentName });
}

/**
 * Helper function for creating ast functions
 *
 * Example:
 * ```
 * // Rename all foos to bars
 * function fooToBarVisitor() {
 *   return createVisitor('Identifier', {
 *     foo(path) {
 *       path.node.name = 'bar';
 *     }
 *   });
 * }
 * ```
 * @param {*} type
 * @param {*} nameHandlers
 * @param {*} nameFn
 */
function createVisitor(type, nameHandlers, nameFn = x => x.node.name) {
  return {
    [type](path) {
      const name = nameFn(path);

      const handler = nameHandlers[name];

      if (handler) {
        handler(path);
      }
    },
  };
}

function createQueryVisitor(type, queryFn = () => true) {
  const results = [];

  const visitor = {
    [type](path) {
      if (queryFn(path)) {
        results.push(path);
      }
    },
  };

  return {
    visitor,
    results,
  }
}

function createStringLiteral(value, quote = "'") {
  return {
    ...t.stringLiteral(value),
    extra: {
      raw: `${quote}${value.replace(quote, `\\${quote}`)}${quote}`,
      rawValue: value,
    },
  };
}

function queryFile(filePath, type, queryFn = () => true) {
  const { visitor, results } = createQueryVisitor(type, queryFn);

  traverseFile(filePath, visitor);

  return results;
}

function query(parent, type, queryFn = () => true) {
  const { visitor, results } = createQueryVisitor(type, queryFn);

  parent.traverse(visitor);

  return results;
}

function getRoot(path) {
  return path.scope.getProgramParent().path;
}

module.exports = {
  createVisitor,
  createStringLiteral,
  isRHSOfMemberExpression,
  getRoot,
  query,
  queryFile,
};
