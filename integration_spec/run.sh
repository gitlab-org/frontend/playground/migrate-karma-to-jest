#!/bin/bash
clean_up() {
  rm -rf $dir/__temp
}

dir=$(dirname $0)

## Setup
clean_up

## Act
yarn start $dir/input $dir/__temp --root-dir "$dir"

## Assert
diff -ur $dir/expected $dir/__temp
diff_pass=$?

## Clean up
clean_up

if [[ "$diff_pass" -eq 0 ]]; then
  echo "--------------"
  echo "  success :)  "
  echo "--------------"
  exit 0
else
  echo "-----------"
  echo "  fail :(  "
  echo "-----------"
  exit 1
fi
