const path = require('path');
const fs = require('fs').promises;
const _ = require('lodash');
const readdirRecursive = require('./utils/readdirRecursive');
const resolver = require('./utils/resolver');
const migrate = require('./migrate');

function replaceDir(path, from, to) {
  return path.replace(new RegExp(`^${from}`), to);
}

function getDefaultWebpackConfigPath(options = {}) {
  return path.join(options.rootDir || '', 'config', 'webpack.config.js');
} 

function writeFile(filePath, content) {
  return fs.mkdir(path.dirname(filePath), { recursive: true })
    .then(() => fs.writeFile(filePath, content, { encoding: 'utf8' }));
}

function isMigratable(filePath) {
  return /\.js$/.test(filePath);
}

function migrateFile(inputPath, outputPath) {
  return fs.readFile(inputPath, { encoding: 'utf8' })
    .then(content => {
      const outputContent = isMigratable(inputPath) ? migrate(content, inputPath) : content;

      return writeFile(outputPath, outputContent);
    });
}

function main(inputPath, outputPath, options = {}) {
  if (!inputPath) {
    return Promise.reject('input path is required!');
  }

  const { 
    webpackConfig: webpackConfigPath = getDefaultWebpackConfigPath(options),
  } = options;

  resolver.init(webpackConfigPath);

  const inputAbs = path.resolve(inputPath);
  const outputAbs = path.resolve(outputPath);

  return fs.stat(inputAbs)
    .then(stats => {
      if (stats.isFile()) {
        return [{ source: inputAbs, dest: outputAbs }];
      } else {
        return readdirRecursive(inputAbs).then(children => 
          children.map(({ name }) => ({ 
            source: name,
            dest: replaceDir(name, inputAbs, outputAbs),
          }))
        );
      }
    })
    .then(jobs => {
      const migrations = jobs.map(({ source, dest }) => migrateFile(source, dest));

      Promise.all(migrations);
    });
}

module.exports = main;
