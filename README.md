# migrate-karma-to-jest

**DEPRECATED:** Please consider using [`jestodus-codemod`](https://gitlab.com/gitlab-org/frontend/playground/jestodus-codemod) instead.

**Why should I not use this?**

You **can** use this if you want... There's just some things wrong with the approach taken with this script.

- Does not preserve whitespace
- Incomplete migration of Jest globals

The [`jestodus-codemod`](https://gitlab.com/gitlab-org/frontend/playground/jestodus-codemod) is a much nicer approach since it uses `jscodeshift` instead of `babel` natively. It also borrows some community codemods for migrating to Jest.

**PLEASE NOTE:** The `jestodus-codemod` is not perfect so some manual interventions are required. Contributions are welcome :) 

----------

This is the home for a script that migrates Karma tests in gitlab-ce/ee to Jest.

<img src="karma-to-jest.png">

### What do I need to run this?

- **Node 12.4** - Otherwise you might see a warning about `the fs.promises API is experimental`.
- **Yarn**

### How do I run this thing?

It's recommended to install this package globally. If you have checked out this repository, you can do this by running: 

```
yarn global add file:$PWD
```

Once installed, the command is:

```
migrate-karma-to-jest <input_dir> <output_dir>
```

After running the script, you'll want to run prettier and eslint to clean up the output files.

### How do I test this thing?

Simply run the integration test script. If there's no `diff` output, then the test has passed :thumbsup:

```
./integration_spec/run.sh
```
